//********************************
// Graham Tufts and Clayton Storms
// CMPSC 111
// 23 February 2017
// Lab 6
//
// Creating DNA Compliments and Mutations
//********************************

import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class ManipulateDNA{
	public static void main(String[] args){
		System.out.println("Clayton Storms and Graham Tufts\nLab #6\n" + new Date() + "\n");
	
		//variables
		String dnaString;

		Scanner scan = new Scanner(System.in);


		//program

		System.out.println("Please enter a DNA string");
		dnaString = scan.nextLine();
		
		System.out.println("Your DNA string is: " + dnaString);
		
		String dnaString1 = dnaString.replace('A', 't');
		String dnaString2 = dnaString1.replace('T', 'a');
		String dnaString3 = dnaString2.replace('C', 'g');
		String dnaString4 = dnaString3.replace('G', 'c');
		String dnaString5 = dnaString4.toUpperCase();
		System.out.println("Output: " + dnaString5);
	}
}

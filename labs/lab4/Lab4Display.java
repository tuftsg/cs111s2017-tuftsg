//==========================================
//Graham Tufts
//All work is my own
//2/9/17
//Lab 4
//Paints the Macintosh Logo
//==========================================

import javax.swing.*;

public class Lab4Display{
  
	public static void main(String[] args){
		JFrame window = new JFrame("Graham Tufts");

		// Add the drawing canvas and do necessary things to
		// make the window appear on the screen!

		window.getContentPane().add(new Lab4());
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setVisible(true);
		window.setSize(400, 400);
	}
}


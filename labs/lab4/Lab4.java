//=================================================
//All work is my own
//Graham Tufts
//2/9/17
//Lab 4
//Paints the Macintosh Logo
//=================================================
import java.awt.*;
import javax.swing.JApplet;

public class Lab4 extends JApplet{
  //-------------------------------------------------
  // Use Graphics methods to add content to the drawing canvas
  //-------------------------------------------------
	public void paint(Graphics page){
		int startY = 350;
		int startX = 100;
		int shiftY = 50;
		int shiftX = 25;
		page.setColor(Color.blue);
		page.fillRect(startX, startY, 200, 50);
		page.setColor(Color.magenta);
		page.fillRect(startX - shiftX, startY - shiftY, 250, 50);
		page.setColor(Color.red);
		page.fillRect(startX - (shiftX * 2), startY - (shiftY * 2), 300, 50);
		page.setColor(Color.orange);
		page.fillRect(startX - (shiftX * 2), startY - (shiftY * 3), 300, 50);
		page.setColor(Color.yellow);
		page.fillRect(startX - shiftX, startY - (shiftY * 4), 250, 50);
		page.setColor(Color.green);
		page.fillRect(startX, startY - (shiftY * 5), 200, 50);
		page.fillArc(190, 25, 50, 50, 75, 135);
		page.fillArc(175, 15, 50, 50, 250, 135);
		page.setColor(Color.white);
		page.fillOval(275, 125, 100, 175);
		page.fillRect(325, 250, 50, 50);
	}
}	

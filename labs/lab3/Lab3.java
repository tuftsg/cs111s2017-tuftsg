
//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Your Name: Graham Tufts
// CMPSC 111 Spring 2017
// Lab # 3
// Date: 2/2/2017
//
// Purpose: Calculating a tip and spliting the bill for a restraunt bill
//*************************************
import java.util.Scanner;

public class Lab3
{

	public static void main(String[] args)
	{
		System.out.println("Graham Tufts, Lab 3, 2/2/2017");
		Scanner scan = new Scanner(System.in);
		System.out.println("What is your name?   ");
		String name = scan.nextLine();
		System.out.println("Hello " + name + ", how much is your bill?   ");
		double bill = scan.nextDouble();
		System.out.println("What percent (from 1 to 100) do you want to tip?   ");
		double percent = scan.nextDouble();
		double tip = (percent / 100) * bill;
		double totalBill = tip + bill;
		System.out.println("Original bill:   " + bill + "\nTip amount:   " + tip + "\nTotal bill:   " + totalBill);
		System.out.println("How many people are splitting the bill?   ");
		int split = scan.nextInt();
		double splitBill = totalBill / split;
		System.out.println("Your final bill is $" + splitBill + " per person, have a nice day!");
	}
}

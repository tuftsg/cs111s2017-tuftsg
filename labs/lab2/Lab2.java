
//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Your Name: Graham Tufts
// CMPSC 111 Spring 2017
// Lab # 2
// Date: 01 26 2017
//
// Purpose: To convert the average American male and female height to meters, and compare that data to NBA and WNBA averages
//*************************************
import java.util.Date; // needed for printing today’s date

public class Lab2{
	//----------------------------
	// main method: program execution begins here
	//----------------------------
	public static void main(String[] args)	{
		// Label output with name and date:
		System.out.println("\nGraham Tufts\nLab #2\n" + new Date() + "\n");
		// Variable dictionary:
		double averageMaleInches;
		double averageFemaleInches;
		double inchesToMeters;
		int averageNBAInches = 79;
		int averageWNBAInches = 72;
		averageMaleInches = 69.7;
		averageFemaleInches = 63.8;
		// Conversions of inches to meters
		inchesToMeters = (1/39.3701);
		double averageMaleMeters = (averageMaleInches * inchesToMeters);
		double averageFemaleMeters = (averageFemaleInches * inchesToMeters);
		double averageNBAMeters = (averageNBAInches * inchesToMeters);
		double averageWNBAMeters = (averageWNBAInches * inchesToMeters);
		// Prints out data
		System.out.println("Average American Male Height: " + averageMaleInches + " inches or " + averageMaleMeters + " meters");
		System.out.println("\nAverage American Female Height in Inches: " + averageFemaleInches + " inches or " + averageFemaleMeters + " meters");
		System.out.println("\nDifference Between Average Man and NBA: " + (averageNBAInches - averageMaleInches) + " inches or " + ((averageNBAMeters * 100) - (averageMaleMeters * 100)) + " centimeters");
		System.out.println("\nDifference Between Average Woman and WNBA: " + (averageWNBAInches - averageFemaleInches) + " inches or " + ((averageWNBAMeters * 100) - (averageFemaleMeters * 100)) + " centimeters\n");
	}
}

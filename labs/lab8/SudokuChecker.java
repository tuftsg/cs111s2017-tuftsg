//*******************************************************
// Graham Tufts
// CMPSC 111
// 30 March, 2017
// Lab 8
//
// Creating a 4x4 Sudoku Board and Checking its Validity
// All Work is my Own
//*******************************************************

import java.util.Scanner;

public class SudokuChecker{

	// Declaring Variables
	Scanner scan = new Scanner(System.in);
	private int w1, w2, w3, w4, x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4, Row1, Row2, Row3, Row4, Col1, Col2, Col3, Col4, Sec1, Sec2, Sec3, Sec4;

	// Constructor
	public SudokuChecker(){
		w1 = 0;
		w2 = 0;
		w3 = 0;
		w4 = 0;
		x1 = 0;
		x2 = 0;
		x3 = 0;
		x4 = 0;
		y1 = 0;
		y2 = 0;
		y3 = 0;
		y4 = 0;
		z1 = 0;
		z2 = 0;
		z3 = 0;
		z4 = 0;

	}

	// Method to Fill Board
	public void inputGrid(){
		System.out.println("Enter your board one cell at a time, from the top right.");
		System.out.print("Enter Cell 1: ");
		w1 = scan.nextInt();
		System.out.print("Enter Cell 2: ");
		w2 = scan.nextInt();
		System.out.print("Enter Cell 3: ");
		w3 = scan.nextInt();
		System.out.print("Enter Cell 4: ");
		w4 = scan.nextInt();
		System.out.print("Enter Cell 5: ");
		x1 = scan.nextInt();
		System.out.print("Enter Cell 6: ");
		x2 = scan.nextInt();
		System.out.print("Enter Cell 7: ");
		x3 = scan.nextInt();
		System.out.print("Enter Cell 8: ");
		x4 = scan.nextInt();
		System.out.print("Enter Cell 9: ");
		y1 = scan.nextInt();
		System.out.print("Enter Cell 10: ");
		y2 = scan.nextInt();
		System.out.print("Enter Cell 11: ");
		y3 = scan.nextInt();
		System.out.print("Enter Cell 12: ");
		y4 = scan.nextInt();
		System.out.print("Enter Cell 13: ");
		z1 = scan.nextInt();
		System.out.print("Enter Cell 14: ");
		z2 = scan.nextInt();
		System.out.print("Enter Cell 15: ");
		z3 = scan.nextInt();
		System.out.print("Enter Cell 16: ");
		z4 = scan.nextInt();
		Row1 = w1 + w2 + w3 + w4;
		Row2 = x1 + x2 + x3 + x4;
		Row3 = y1 + y2 + y3 + y4;
		Row4 = z1 + z2 + z3 + z4;
		Col1 = w1 + x1 + y1 + z1;
		Col2 = w2 + x2 + y2 + z2;
		Col3 = w3 + x3 + y3 + z3;
		Col4 = w4 + x4 + y4 + z4;
		Sec1 = w1 + w2 + x1 + x2;
		Sec2 = w3 + w4 + x3 + x4;
		Sec3 = y1 + y2 + z1 + z2;
		Sec4 = y3 + y4 + z3 + z4;
	}

	// Method to Check Board's Validity
	public void checkGrid(){
		if(Row1 == 10 && Row2 == 10 && Row3 == 10 && Row4 == 10){
			if(Col1 == 10 && Col2 == 10 && Col3 == 10 && Col4 == 10){
				if(Sec1 == 10 && Sec2 == 10 && Sec3 == 10 && Sec4 == 10){
					System.out.println("Sudoku is Valid.");
				}
				else
					System.out.println("Sudoku is Invalid.");
			}
			else
				System.out.println("Sudoku is Invalid.");
		}
		else
			System.out.println("Sudoku is Invalid.");
	}
}

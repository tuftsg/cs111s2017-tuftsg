//*******************************************************
// Graham Tufts
// CMPSC 111
// 30 March, 2017
// Lab 8
//
// Creating a 4x4 Sudoku Board and Checking its Validity
// All Work is my Own
//*******************************************************

import java.util.Date;

public class SudokuCheckerMain{

	public static void main(String args[]){
		SudokuChecker checker = new SudokuChecker();
		System.out.println("Graham Tufts\nLab #8\n" + new Date() + "\n");
		checker.inputGrid();
		checker.checkGrid();
	}
}

//********************************
// Graham Tufts
// CMPSC 111
// 19 January 2017
// Lab 1
//
// Listing 1.1 from Lewis & Loftus, slightly modified.
// Demonstrates the basic structure of a Java application.
//********************************

import java.util.Date;

public class Lab1{
	
	public static void main(String[] args){
		System.out.println("\nGraham Tufts " + new Date());
		System.out.println("Lab 1");

		//--------------------------------------
		System.out.println("\nMan who run in front of car get tired");
		System.out.println("Man who run behind car get exhausted");
		// -------------------------------------

		System.out.println("\nAdvice from Graham Tufts\n");
	}
}

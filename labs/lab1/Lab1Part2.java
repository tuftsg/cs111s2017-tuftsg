// Graham Tufts
// CMPSC 111
// 19 January 2017
// Lab 1 Part 2

import java.util.Date;

public class Lab1Part2{

	public static void main(String[] args){
		String name = "Graham Tufts";
		String date = "" + new Date() + "";
		System.out.println("\n" + name + " " + date);
		System.out.println("The Times They Are a-Changin': Verse 2\n");
		System.out.println("Come writers and critics who prophesize with your pen");
		System.out.println("And keep your eyes wide the chance won't come again");
		System.out.println("And don't speak too soon for the wheel's still in spin");
		System.out.println("And there's no telling who that it's naming");
		System.out.println("For the loser now will be later to win");
		System.out.println("Cause the times they are a-changing");
		System.out.println("\nBob Dylan, 1963\n");
	}
}

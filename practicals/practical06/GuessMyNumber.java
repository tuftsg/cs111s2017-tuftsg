//********************************
// Practical06
// Graham Tufts
// Computer Science 111
//********************************

import java.util.Date;
import java.util.Scanner;
import java.util.Random;

public class GuessMyNumber{
	
	public static void main(String[] args){
		System.out.println("Graham Tufts, Practical06, " + new Date());
		
		Random ran = new Random();
		Scanner scan = new Scanner();
		int num = ran.nextInt(100) + 1;
		int num1;
		int count = 0;
		boolean check = false;

		while(check == false){
			System.out.println("Guess a number between 1 and 100.");
			num1 = scan.nextInt();
			count ++;
			if(num1 == num)
				System.out.println("You've guessed the correct number!\nIt took you " + count + "tries to guess it correctly!");
			else if(num1 < num)
				System.out.println("Too low!");
			else
				System.out.println("Too high!");
		}
	}
}

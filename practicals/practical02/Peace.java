// Peace
// Graham Tufts
// Computer Science 111
// 1/30/2017

public class Peace{

	public static void main(String[] args){
		System.out.println("   _                 _");
		System.out.println("  | \\               / |\n  \\  \\             /  /\n   \\  \\           /  /\n    \\  \\         /  /\n     \\  \\       /  /\n      \\  \\     /  /\n       \\  \\   /  / __   __\n        \\  \\_/  /_/  \\_/  \\");
		System.out.println("        /                  )");
		System.out.println("        |                  |\n        |                  |\n        |                  /\n        |                 /\n        |                /\n        |               /");	
	}
}

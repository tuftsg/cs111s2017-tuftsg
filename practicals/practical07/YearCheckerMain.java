//*****************************
// CMPSC 111
// Practical 8
// October 30, 2015
//
// Purpose: a program that determines what activities happen
// during a specific year.
//*****************************

import java.util.Date;
import java.util.Scanner;

public class YearCheckerMain
{
    public static void main ( String[] args)
    {
        //Variable dictionary
        Scanner scan = new Scanner(System.in);
        int userInput;

        System.out.println("Please enter a year between 1000 and 3000!");
        userInput = scan.nextInt();

        YearChecker activities = new YearChecker(userInput);

        if(activities.isLeapYear())
		System.out.println(userInput + " is a leap year.");
	else
		System.out.println(userInput + " isn't a leap year.");

	if(activities.isCicadaYear())
		System.out.println("It's an emergence year for Brood II of the 17-year cicadas.");
	else
		System.out.println("It's not an emergence year for cicadas.");

	if(activities.isSunspotYear())
		System.out.println("It's a peak sunspot year.");
	else
		System.out.println("It's not a peak sunspot year.");

        System.out.println("Thank you for using this program.");
    }
}
